const httpStatus = require('http-status');
const knex = require('../knexClient');

exports.getBook = async (req, res, next) => {
  try {
    const bookId = req.params.bookId;
    const books = await knex
    .select('id','name')
    .from('books')
    .where('id',bookId);
    if (!books || books.length < 1){
      res.status(httpStatus.NO_CONTENT).end();
      return;
    };
    res.send(books[0]);
  } catch (error) {
    return next(error);
  }
}

exports.getBooks = async (req, res, next) => {
  try {
    const books = await knex
    .select('id','name')
    .from('books');
    if (!books || books.length < 1){
      res.status(httpStatus.NO_CONTENT).end();
      return;
    };    
    res.send(books);
  } catch (error) {
    return next(error);
  }
};

exports.createBooks = async (req, res, next) => {
  try {
    const name = req.body.name;
    await knex('books').insert([
      {
        name:name
      }
    ]);
    res.status(httpStatus.CREATED).end();
  } catch (error) {
    return next(error);
  }
};