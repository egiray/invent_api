const httpStatus = require('http-status');
const knex = require('../knexClient');

exports.getUser = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    const users = await knex
    .select('id','name')
    .from('users')
    .where('id',userId);

    if (!users || users.length < 1){
      res.status(httpStatus.NO_CONTENT).end();
      return;
    };
    const user = users[0]
    user['books'] = {};
    user['books']['past'] = [];
    user['books']['present'] = [];

    bookings = await knex
    .select({userScore: 'borrowings.score'},'books.name')
    .from('borrowings')
    .join('books', {'books.id': 'borrowings.book_id'})
    .where('borrowings.user_id',user.id)
    if (bookings.length > 0) {
      bookings.forEach(booking => {
        if (booking['userScore']){
          user['books']['past'].push(booking);
        }else {
          delete booking.userScore;
          user['books']['present'].push(booking);
        }
      });
    }
    res.send(users[0]);
  } catch (error) {
    return next(error);
  }
}

exports.getUsers = async (req, res, next) => {
  try {
    const users = await knex
    .select('id','name')
    .from('users');
    if (!users || users.length < 1){
      res.status(httpStatus.NO_CONTENT).end();
      return;
    };    
    res.send(users);
  } catch (error) {
    return next(error);
  }
};

exports.createUsers = async (req, res, next) => {
  try {
    const name = req.body.name;
    await knex('users').insert([
      {
        name:name
      }
    ]);
    res.status(httpStatus.CREATED).end();
  } catch (error) {
    return next(error);
  }
};


exports.borrowBook = async (req, res, next) => {
  try {
    const { userId, bookId} = req.params;
    // Checking if this book is available for booking
    const alreadyBooked = await knex('borrowings')
    .select(['user_id', 'book_id'])
    .whereNull('score')
    .andWhere('book_id', bookId);
    if (alreadyBooked.length > 0){
      res.status(httpStatus.NOT_ACCEPTABLE);
      res.send('This book is already booked');
      return;
    }

    await knex('borrowings').insert([
      {
        user_id: userId,
        book_id: bookId
      }
    ]);
    res.status(httpStatus.CREATED).end();
  } catch (error) {
    return next(error);
  }
};


exports.returnBook = async (req, res, next) => {
  try {
    const { userId, bookId} = req.params;
    const score = req.body.score;
    const updated = await knex('borrowings').update('score',score)
    .whereNull('score')
    .andWhere('user_id', userId)
    .andWhere('book_id', bookId);
    if (updated < 1) {
      res.status(httpStatus.NOT_ACCEPTABLE);
      res.send('No such booking!');
      return;
    }
    res.status(httpStatus.NO_CONTENT).end();
  } catch (error) {
    return next(error);
  }
};