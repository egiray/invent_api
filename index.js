'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const helmet = require('helmet');
const app = express();
const error = require('./middlewares/error');
const { port, env } = require('./config/vars');
const routes = require('./routes');
const logger = require('./config/logger');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compress());
app.use(methodOverride());
app.use(helmet());
app.use(cors());


app.use('/', routes);
// if error is not an instanceOf APIError, convert it.
app.use(error.converter);
// catch 404 and forward to error handler
app.use(error.notFound);
// error handler, send stacktrace only during development
app.use(error.handler);

app.listen(port, () => logger.info(`server started on port ${port} (${env})`));

module.exports = app;