exports.up = function(knex) {
  return knex.schema.createTable('borrowings', table => {
    table.increments()
    table.string('user_id').references('users.id');
    table.string('book_id').references('books.id');
    table.string('score')
  })  
};

exports.down = function(knex) {
  return knex.schema.dropTable('borrowings')
};
