const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/books.controller');
const {
  getBook,
  createBook,
} = require('../validations/books.validation');

const router = express.Router();

router.route('/:bookId').get(validate(getBook), controller.getBook);
router.route('/').get(controller.getBooks);
router.route('/').post(validate(createBook), controller.createBooks);


module.exports = router;
