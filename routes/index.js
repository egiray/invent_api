const express = require('express');
const usersRoutes = require('./users.route');
const booksRoutes = require('./books.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET v1/docs
 */
router.use('/docs', express.static('docs'));

router.use('/users', usersRoutes);
router.use('/books', booksRoutes);

module.exports = router;
