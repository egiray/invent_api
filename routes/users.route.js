const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/users.controller');
const {
  getUser,
  createUser,
  borrowBook,
  returnBook
} = require('../validations/users.validation');

const router = express.Router();

router.route('/:userId').get(validate(getUser), controller.getUser);
router.route('/').get(controller.getUsers);
router.route('/').post(validate(createUser), controller.createUsers);

router.route('/:userId/borrow/:bookId').post(validate(borrowBook), controller.borrowBook);
router.route('/:userId/return/:bookId').post(validate(returnBook), controller.returnBook);


module.exports = router;
