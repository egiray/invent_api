const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const { some } = require('lodash');
const app = require('../../index');

describe('Books API', async () => {
  beforeEach(async () => {
    book = {
      name: 'Neuromancer',
    };
  });
  describe('POST /books', () => {
    it('should create a new book when request is ok', () => {
      return request(app)
        .post('/books')
        .send(book)
        .expect(httpStatus.CREATED)
    });
  });
  describe('GET /books', () => {
    it('getBooks should include book named Neuromancer', () => {
      return request(app)
        .get('/books')
        .expect(httpStatus.OK)
        .then(res => {
          const includesBook = some(res.body, book);
          expect(includesBook).to.be.true;
        })
    });
  });
  describe('GET /books/1', () => {
    it('getBook should include book named Neuromancer', () => {
      return request(app)
        .get('/books/1')
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body).to.include({id:1, ...book});
        })
    });
  });  
});
