const request = require('supertest');
const httpStatus = require('http-status');
const {
  expect
} = require('chai');
const {
  some
} = require('lodash');
const app = require('../../index');

describe('Users API', async () => {
  beforeEach(async () => {
    user = {
      name: 'Esin Öner'
    };
    borrowing = {
      score: '9'
    };
    book = {
      name: 'Neuromancer'
    };
  });
  describe('POST /users', () => {
    it('should create a new user when request is ok', () => {
      return request(app)
        .post('/users')
        .send(user)
        .expect(httpStatus.CREATED)
    });
  });
  describe('GET /users', () => {
    it('getUsers should include user named Esin Öner', () => {
      return request(app)
        .get('/users')
        .expect(httpStatus.OK)
        .then(res => {
          const includesUser = some(res.body, user);
          expect(includesUser).to.be.true;
        })
    });
  });
  describe('GET /users/1', () => {
    it('getUser should include user named Esin Öner', () => {
      return request(app)
        .get('/users/1')
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.name).to.be.equal(user.name);
        })
    });
  });
  describe('POST /users/1/borrow/1', () => {
    it('Borrow Book should create new borrowing', () => {
      return request(app)
        .post('/users/1/borrow/1')
        .expect(httpStatus.CREATED)
    });
    it('Borrowing should be seen in users books', () => {
      return request(app)
        .get('/users/1')
        .then(res =>{
          expect(res.body.books.present[0].name).to.be.equal(book.name);
        })
        
    })
    it('Book cannot be borrowed before it\'s returned', () => {
      return request(app)
        .post('/users/1/borrow/1')
        .expect(httpStatus.NOT_ACCEPTABLE)
    })
  });

});
describe('POST /users/1/return/1', () => {
  it('Return book should move book into present books of user', () => {
    return request(app)
      .post('/users/1/return/1')
      .send((borrowing))
      .expect(httpStatus.NO_CONTENT)
      .then(res => {
        request(app).get('/users/1')
          .then(res => {
            expect(res.body.books.past[0].name).to.be.equal(book.name);
          })
      })
  });
});
describe('POST /users/200/return/1', () => {
  it('Return Book should return error when an invalid user_id is used ', () => {
    return request(app)
      .post('/users/200/return/1')
      .send((borrowing))
      .expect(httpStatus.NOT_ACCEPTABLE)
  })
});

describe('POST /users/1/return/2000', () => {
  it('Return Book should return error when an invalid book_id is used ', () => {
    return request(app)
      .post('/users/1/return/2000')
      .send((borrowing))
      .expect(httpStatus.NOT_ACCEPTABLE)
  });
});