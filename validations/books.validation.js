const Joi = require('joi');

module.exports = {
  getBook: {
    params: {
      bookId: Joi.number().required(),
    },
  },
  createBook: {
    body: {
      name: Joi.string().required(),
    },
  },
};
