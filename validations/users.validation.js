const Joi = require('joi');

module.exports = {
  getUser: {
    params: {
      userId: Joi.number().required(),
    },
  },
  createUser: {
    body: {
      name: Joi.string().required(),
    },
  },
  borrowBook: {
    params: {
      userId: Joi.number().required(),
      bookId: Joi.number().required(),
    },
  },
  returnBook: {
    params: {
      userId: Joi.number().required(),
      bookId: Joi.number().required(),
    },
    body: {
      score: Joi.number().required(),
    }
  },  
};
